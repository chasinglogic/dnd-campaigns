# Pillars of Eternity Campaign

### Backstory

1.  Prophet's War
    
    The Prophet's war happened in the past but is recent enough that
    it's fresh in the minds of the people who lived in the Dyrwood
    during the war. Approx 5 - 10 years ago. Waidwen was a prophet of
    Eothas. Whether he was a real prophet or not is still up for debate
    however he rose to power through his popularity from his good deeds.
    Once in power however Waidwen became cruel and ruthless waging wars
    of "purification" on the surrounding areas and killing his own
    people. Those who sided with Eothas are regarded with scorn and
    mistrust after the war. Waidwen was slain by the Eleven weilding the
    "God Hammer" a bomb that was crafted by the priests of Magren.

### Gilded Vale
### Defiance Bay

1.  First Fires
    
    1.  Quest Locations
        
        1.  Crucible Keep
            
            The Crucible Knights call this keep home.
            
            1.  Important NPC's
                
                1.  Dunstan
                    
                    Forgemaster of the keep, is sort of anti-animancy
                    but values loyalty to Commander Clyver much higher
                    than his own hatred of it. So when ordered to make
                    the "animancy robocops" will do so.
                
                2.  Commander Clyver
                    
                    Commander Clyver is a noble who cares more about the
                    lineage of the Crucible Knights than their
                    effectiveness, in response to the Dozens rioting he
                    will commission for their to be animancy robocops as
                    discovered by a member of the Leaden Key
                
                3.  Wyla
                    
                    Wyla is an instructor for the Crucible Knights and
                    cares a lot about the new recruits. She will tell
                    the adventurers about some missing sentries in
                    Heritage Hill and ask the adventurers to find them
                    see Banshee In Heritage Hill for details.
        
        2.  Udcal Palace

2.  Heritage Hill
    
    1.  Quest Locations
        
        1.  Heritage Hill
            
            Heritage Hill is the largest encounter the adventurers will
            have run into yet.
        
        2.  The Banshee on Heritage Hill

3.  Brackenbury District
    
    1.  Quest Locations
        
        1.  Hadret House (Dunryd Row)
            
            1.  Notable NPC's
                
                1.  Lady Webb
                    
                    Lady Webb is an extremely old woman who runs Dunryd
                    Row a pseudo-secret society of detectives / spies
                    who influence politics via subterfuge and sell
                    information. Lady Webb knows Thaos personally as
                    they were once lovers and knows quite a bit about
                    the Leaden Key, she has been trying to understand
                    their true purpose for some time and working against
                    them she is certain their activities are to the
                    detriment of everyone. She is certain that the
                    Leaden Key is anti-animancy.
                
                2.  Kurren
                    
                    Is a Ciper and Detective who works for Dunryd Row,
                    and is Lady Webb's second in command.
        
        2.  House Doemenel
            
            House doemenel is a "thieves guild" of sorts. They are all
            disgraced Aedyran nobles who are trying to regain the power
            that they lost during the Dyrwoodan rebellion before the
            Prophet's War

4.  Copperlane Marketplace
    
    1.  Places of Interest
        
        1.  Lora's Mystical Goods
            
            Magic Store
        
        2.  Narmer's Sundries
            
            General adventuring gear
        
        3.  Igrun's Arms and Armor
            
            Weapons / Armor
        
        4.  Peregrund's Goods
            
            Optional can carry anything you would like to give to the
            party
    
    2.  Quest Locations
        
        1.  Catacombs
        
        2.  Goose and Fox
        
        3.  The Hall of Revealed Mysteries

5.  Ondra's Gift
    
    Docks the Dozens are stationed out of here and are generally hostile
    to the party.

### Twin Elms

This city of Glanfathan tribes, was established between a pair of
enormous ancient elm trees, a location chosen for its proximity to the
Gods. Reminiscent of adra stones, the elm trees exhibit a strange
properties of soul affinity, they are attended by a pair of dryad
sisters in vigil that predates the city itself. Set on a southeastern
shore of lake, it is built around a major ancient Engwithan
ruin\[2\]\[1\], its a unique mixture of ruined architecture of adra
foundations with a layer of Viking-inspired Glanfathan buildings built
into it.\[3\] Oldest among the structures in the city is Teir Evron, the
Hall of Stars, a stone tower whose pinnacle is used for astrology and
divine communication.\[1\] The permanent settlement is used as gathering
point for the semi-nomadic people. The lives of its populace are pretty
much dominated by religion and temples.\[2\] Six major Glanfathan tribes
are responsible for governing the city: Keepers of the Stone, Stone
Bramble, Fisher Crane, Three-Tusk Stelgaer, The Guided Compass and
Twice-Split Arrows.\[1\]

1.  Keepers of the Stone
    
    The Keepers of the Stone are one of the six Glanfathan tribes
    governing Twin Elms. Considering themselves the oldest Glanfathan
    tribe, and claiming to be the first to have made contact with the
    Engwitans, they guard an ancient adra stone they claim was a gift
    from them. Their numbers are spread through out the Eír Glanfath,
    but they are reluctant to engage with outsiders and marry within the
    tribe.\[1\]

2.  Stone Bramble

3.  Fisher Crane

4.  Three-Tusk Stelgaer

5.  The Guided Compass

6.  Twice-Split Arrow

7.  Teir Evron

